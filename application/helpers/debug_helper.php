<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('debug'))
{
    function debug($var)
    {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }   
}

if ( ! function_exists('debug_r'))
{
    function debug_r($var)
    {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }   
}

/* End of file debug.php */
/* Location: ./application/controllers/debug.php */