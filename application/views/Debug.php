<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lab | Sandbox</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url('css/handsontable/jquery.handsontable.full.min.css')?>">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="example"></div>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Plugins -->
    <script src="<?=base_url('js/plugins/handsontable/jquery.handsontable.full.min.js')?>"></script>
    <script src="<?=base_url('js/plugins/handsontable/numeral.js')?>"></script>
    
    <script type="text/javascript">

      var labelNomor = "No.";
      var labelKomoditas = "Komoditas";
      function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
          Handsontable.renderers.TextRenderer.apply(this, arguments);
          td.style.fontWeight = 'bold';
          td.style.color = 'green';
          td.style.background = '#CEC';
          td.style.textAlign = 'center';
      }

      function firstColRenderer(instance, td, row, col, prop, value, cellProperties) {
          Handsontable.renderers.TextRenderer.apply(this, arguments);
          td.style.textAlign = 'center';
      }

      $(function(){
        var $container = $('#example');
        var request = $.ajax({
          url: "<?=site_url("request/isiTabel/1")?>",          
          dataType: "json",          
        });

        request.done(function(data) {
            var kolom = new Array();
            var konfigurasiKolom = new Array();
            var komoditas = new Array();
            var dataset = new Array();
                dataset[0] = new Array();

            console.log(data);

            for(i=0; i<data.kolom.length+2; i++){
              if(i==0){
                kolom.push(labelNomor);
              }else if(i==1){
                kolom.push(labelKomoditas);
              }else{                
                kolom.push(data.kolom[i-2].nama);                
              }
              dataset[0].push(i+1);
            }

            for(i=0; i<data.komoditas.length; i++){
              row = [];
              row[0] = i+1;
              row[1] = data.komoditas[i].nama;
              komoditas.push(row); 
            }

            $container.handsontable({  
              data : dataset,            
              colHeaders: kolom,
              minCols: kolom.length,
              maxCols: kolom.length,                          
              afterInit:function(){ 
                var $table = $container.data('handsontable');
                for (i = 0; i < komoditas.length; i++) {
                    dataset.push(komoditas[i]);
                }
                $table.render();
              },
              cells: function(row, col) {
                  var cellProperties = {};
                  if (row === 0 || col === 0 || col === 1 || $container.handsontable('getData')[row][col] === 'readOnly') {
                      cellProperties.readOnly = true;
                      cellProperties.textAlign = 'center';
                  }
                  
                  if(col === 0){
                      cellProperties.renderer = firstColRenderer;
                  }
                  
                  if (row === 0) {
                      cellProperties.renderer = firstRowRenderer;
                  }
                              
                  return cellProperties;
              }
            });

            var $table = $container.data('handsontable');
            $table.render();
        });      
                     
      });        
    </script>
  </body>
</html>