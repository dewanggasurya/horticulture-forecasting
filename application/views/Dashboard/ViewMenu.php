<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?=base_url('img/avatar3.png')?>" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p>Hello, Jane</p>

            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..."/>
            <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li>
            <a href="<?=base_url()?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#" title="Rekap. Kabupaten Statistik Pertanian">
                <i class="fa fa-th"></i> 
                <span>RKSP</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?= site_url('rksp/sbs')?>"><i class="fa fa-angle-double-right"></i> Sayuran & Buah Semusim</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Buah & Sayuran Tahunan</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Tanaman Biofarmaka</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Tanaman Hias</a></li>                
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-bar-chart-o"></i>
                <span>Laporan</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Rekap. Provinsi</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Rekap. Kabupaten/Kota</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Rekap. Komoditas</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-bar-chart-o"></i>
                <span>Peramalan</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>            
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Peramalan Produksi</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Konstanta Peramalan</a></li>              
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-folder"></i> <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Pegguna</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Daerah</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Kategori</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Komoditas</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Satuan</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Formulir RKSP</a></li>                
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-table"></i> <span>Tables</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="pages/tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                <li><a href="pages/tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
            </ul>
        </li>
        <li>
            <a href="pages/calendar.html">
                <i class="fa fa-calendar"></i> <span>Log</span>                
            </a>
        </li>
    </ul>
</section>    