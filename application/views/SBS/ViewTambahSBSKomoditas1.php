<!-- Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?=base_url('css/handsontable/jquery.handsontable.full.min.css')?>">

<input type="hidden" value="1" name="id_pengguna" id="id_pengguna"/>
<div class="row">
    <div class="col-lg-8">
      <div class="box box-solid bg-light-blue">
          <div class="box-header">
              <i class="fa fa-file-text"></i>
              <h3 class="box-title">Menentukan informasi utama RKSP SBS</h3>
              <!-- tools box -->
              <div class="pull-right box-tools"></div><!-- /. tools -->
          </div>
          <form class="form-horizontal">
          	<div class="box-body">
  					<div class="row">
  						<div class="col-lg-6">
  							<div class="form-group">
  								<label for="komoditas" class="col-sm-3 control-label">Provinsi</label>
  								<div class="col-sm-9">
  									<input type="hidden" name="id_daerah" id="id_daerah" value="8"/>
  									<p class="form-control-static">Jawa Timur</p>
  								</div>
  							</div>							
  						</div>	
  						<div class="col-lg-6">
  							<div class="form-group">
  								<label for="komoditas" class="col-sm-3 control-label">Tahun</label>
  								<div class="col-sm-9">
  									<select class="form-control" id="tahun" name="tahun">
  										<?=$tahun?>
  									</select>
  								</div>
  							</div>
  						</div>
  					</div>
  					<div class="row">
  						<div class="col-lg-6">						
  							<div class="form-group">
  								<label for="komoditas" class="col-sm-3 control-label">Komoditas</label>
  								<div class="col-sm-9">
  									<select class="form-control" id="komoditas" name="komoditas">
  										<?=$komoditas?>
  									</select>				
  								</div>		
  							</div>
  						</div>	
  						<div class="col-lg-6">
  							<div class="form-group">
  								<label for="bulan"  class="col-sm-3 control-label">Bulan</label>
  								<div class="col-sm-9">
  									<select class="form-control" id="bulan" name="bulan">
  										<?=$bulan?>
  									</select>
  								</div>
  							</div>
  						</div>	
  					</div>						    															
    				</div>	
    				<div class="box-footer clearfix">
    	        <label>
    	          <input type="checkbox" value="1" name="review" id="review"/> Review RKSP sebelum simpan
    	        </label>          
              <span id="first-menu">
                <button type="button" id="lihat-rksp" class="pull-right btn btn-primary" style="margin-left:10px;">Lihat RKSP <i class="fa fa-gears"></i></button>              
                <button type="reset" class="pull-right btn btn-default"><i class="fa fa-trash-o"></i> Reset</button>
              </span>    
              <span id="second-menu" style="display:none;">
                <button type="button" id="simpan-rksp" class="pull-right btn btn-success" style="margin-left:10px;">Simpan RKSP <i class="fa fa-save"></i></button>
                <button type="reset" id="batal-rksp" class="pull-right btn btn-default"><i class="fa fa-trash-o"></i> Batal</button>
              </span>        					
            </div>
		     </form>					
	    </div> 
	</div>
	<div class="col-lg-4">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <i class="fa fa-info-circle"></i>
                <h3 class="box-title">Keterangan RKSP SBS</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">					
				</div><!-- /. tools -->
            </div>
        	<div class="box-body">                				
        	<form id="form-info" class="form-horizontal" role="form" style="display:none;s">
					<div class="form-group">
						<label class="col-sm-5 control-label">Status</label>
						<div class="col-sm-7">
							<p class="form-control-static" id="status">Sudah ada</p>
						</div>
					</div>					
					<div class="form-group">
						<label class="col-sm-5 control-label">Tanggal Dibuat</label>
						<div class="col-sm-7">
							<p class="form-control-static" id="tgl-buat">7 Oktober 2014 15:30 WIB</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label">Tanggal Diubah</label>
						<div class="col-sm-7">
							<p class="form-control-static" id="tgl-ubah">Belum pernah diubah</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label">Pembuat</label>
						<div class="col-sm-7">
							<p class="form-control-static"><a href="#" id="nama-pembuat">Sugeng Priyatno</a></p>
						</div>
					</div>
				</form>
				<p id="text-info" class="help-block" style="text-align:center;padding:48.5px 0px;"><i>Isi form terlebih dahulu untuk melihat informasi RKSP SBS.</i></p>
			</div>					
		</div> 
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-table"></i>
              <h3 class="box-title">Dokumen RKSP</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">					
			        </div><!-- /. tools -->
            </div>
            <div class="box-body">
            	<div class="row">
	            	<div class="col-lg-12">
	            		<div id="sbs">
	            			<i id="notifikasi">Memproses tabel RKSP SBS</i>
                    <div id="table-overlay">
                      <span id="note">Pilih RKSP SBS terlebih dahulu</span>
                    </div>
	            		</div>
	            	</div>
            	</div>
           	</div>
           	<div class="box-footer clearfix">
           		*)Harap cek kembali sebelum melakukan proses penyimpanan data.
           	</div>
        </div>
	</div>
</div>

<!-- Plugins -->
<script src="<?=base_url('js/plugins/handsontable/jquery.handsontable.full.min.js')?>"></script>
<script src="<?=base_url('js/plugins/handsontable/satuan.js')?>"></script>
<script src="<?=base_url('js/plugins/notify/notify.min.js')?>"></script>

<script type="text/javascript">

  var labelNomor = "No.";
  var labelDaerah = "Daerah";
  var $tableOverlay = $('#table-overlay');
  var $formInfo = $('#form-info');
  var $textInfo = $('#text-info');
  var $textInfoInner = $('#text-info i');
  var $firstMenu = $('#first-menu');
  var $secondMenu = $('#second-menu');
  var $table;

  //Button Action
  var $simpanRKSP = $('#simpan-rksp');
  var $batalRKSP = $('#batal-rksp');
  var $lihatRKSP = $('#lihat-rksp');

  //Form
  var $tahun = $('#tahun');
  var $bulan = $('#bulan');
  var $komoditas = $('#komoditas');
  var $daerah = $('#id_daerah');
  var $pengguna = $('#id_pengguna');
  var $notifikasi = $('#notifikasi');
  var $satatus = $('#status');
  var $tglUbah = $('#tgl-ubah');
  var $tglBuat = $('#tgl-buat');
  var $namaPembuat = $('#nama-pembuat');
  var $kategori = 1;  
  var $tingkat = 2;      

  function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.style.fontWeight = 'bold';
      td.style.color = 'green';
      td.style.background = '#CEC';
      td.style.textAlign = 'center';
  }

  function firstColRenderer(instance, td, row, col, prop, value, cellProperties) {
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.style.textAlign = 'center';
  }
  function isEmpty(v){
    if(v === '' || v === null || v === undefined)
      return true;

    return false;
  }
  $(function(){
    var $container = $('#sbs');
    
    //Mengambil nilai default dari tabel    
    var requestIsiDefaultTable = $.ajax({
      type : 'POST',
      url: "<?=site_url("request/isiDefaultTabelDaerah")?>",          
      dataType: "json",      
      data :{kategori: $kategori, tingkat:$tingkat}           
    });

    requestIsiDefaultTable.done(function(data) {
        var kolom = new Array();
        var konfigurasiKolom = new Array();
        var daerah = new Array();
        var dataset = new Array();
            dataset[0] = new Array();
        var satuan = new Array();        

        console.log(data);

        for(i=0; i<data.kolom.length+2; i++){
          if(i==0){
            kolom.push(labelNomor);
            satuan.push({});
          }else if(i==1){
            kolom.push(labelDaerah);
            satuan.push({});
          }else{       
            var nama = data.kolom[i-2].nama;         
            var tipe = data.kolom[i-2].kategori;         
            var kode = data.kolom[i-2].kode_satuan;         

            //Mengatur satuan
            kolom.push(nama);
            if(tipe === "1" || tipe === "2"){
              satuan.push({
                type:'numeric',
                format: '0,0.00 $',
                language: kode
              });
            }else if(tipe === "4"){
              satuan.push({
                type:'numeric',
                format: '$ 0,0.00',
                language: kode
              });
            }else{
              satuan.push({});
            }            
          }
          dataset[0].push(i+1);
        }

        for(i=0; i<data.daerah.length; i++){
          row = [];
          row[0] = i+1;
          row[1] = data.daerah[i].nama;
          daerah.push(row); 
        }      

        //Membuat spreadsheet
        $container.handsontable({  
          data : dataset,            
          colHeaders: kolom,
          minCols: kolom.length,
          maxCols: kolom.length,
          fixedRowsTop: 1,      
          columns: satuan,                
          afterInit:function(){ 
            $table = $container.data('handsontable');
            for (i = 0; i < daerah.length; i++) {
                dataset.push(daerah[i]);
            }
            $table.render();
            $notifikasi.hide();
          },
          cells: function(row, col) {
              var cellProperties = {};
              if (row === 0 || col === 0 || col === 1 || $container.handsontable('getData')[row][col] === 'readOnly') {
                  cellProperties.readOnly = true;
                  cellProperties.textAlign = 'center';
              }
              
              if(col === 0){
                  cellProperties.renderer = firstColRenderer;
              }
              
              if (row === 0) {
                  cellProperties.renderer = firstRowRenderer;
              }
                          
              return cellProperties;
          }
        });

        var panjang = $container.innerWidth();
        var lebar = $container.innerHeight();
        $tableOverlay.css({width:panjang, height:lebar+10});
        $tableOverlay.show();
        //alert(panjang+"x"+lebar);

        /*var $table = $container.data('handsontable');
        $table.render();*/
    });  

  //Proses peyimpanan data RKSP
  $simpanRKSP.click(function(){
      var data = $table.getData();
      var json = JSON.stringify(data);
      console.log(json);
      varTahun = $tahun.val();
      varBulan = $bulan.val();
      varKomoditas = $komoditas.val();
      varIdPengguna = $pengguna.val();

      var requestIsiDefaultTable = $.ajax({
        type : 'POST',
        url: "<?=site_url("request/simpanRKSPDaerah")?>",          
        dataType: "json",      
        data :{tahun: varTahun, bulan:varBulan, komoditas:varKomoditas, pengguna:varIdPengguna, rksp:json, kategori:$kategori, tigkat:$tingkat}
      });

      requestIsiDefaultTable.done(function(response){
        if(response.status === 1){

        }else{

        }
      });

      console.log(json);
    });

    //Proses pembatalan pembuatan RKSP
    $batalRKSP.click(function(){
      $tableOverlay.fadeIn();
      $secondMenu.hide();           
      $firstMenu.fadeIn();
    });

    //Proses melihat RKSP
    $lihatRKSP.click(function(){
      varTahun = $tahun.val();
      varBulan = $bulan.val();
      varKomoditas = $komoditas.val();
      varIdDaerah = $daerah.val();

      if(isEmpty(varTahun) || isEmpty(varBulan) || isEmpty(varKomoditas) || isEmpty(varIdDaerah)){        
        return
      }

      //Proses pengambilan isi tabel RKSP
      var rkspRequest = $.ajax({
        type : 'POST',
        url: "<?=site_url("request/isiTabelDaerah")?>",          
        dataType: "json",
        data :{tahun: varTahun, periode:varBulan, komoditas:varKomoditas, daerah: varIdDaerah, kategori:$kategori}       
      });      

      function toggleButtonSet(){
          $formInfo.fadeOut();
          $textInfo.fadeIn();
          $firstMenu.hide();           
          $secondMenu.fadeIn();  
      }

      rkspRequest.done(function(data){
        if(data.status !== 4){
          $tableOverlay.fadeOut();          
          if(data.status === 3 || data.status === 2){            
            $textInfoInner.html('RKSP belum pernah dibuat sebelumnya'); 
            toggleButtonSet();    
          }else{            
            $textInfo.fadeOut();
            $formInfo.fadeIn();            
            //Memasukkan data yang telah ada kedalam spreadsheet
            var output = data.table;
            var table = new Array();
            for(i=0; i<data.daerah.length; i++){
              row = [];
              row[0] = i+1;
              row[1] = data.daerah[i].nama;
              daerah.push(row); 
            } 
            for(i=0; i<output.length; i++){
               
            }
          }
        }        
      });

    });        
  });        
                      
</script>