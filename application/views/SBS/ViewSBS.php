<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid bg-aqua">
            <div class="box-header">
                <i class="fa fa-plus"></i>
                <h3 class="box-title">Tambah RKSP SBS Baru</h3>
                <div class="pull-right box-tools">                    
                    <a href="<?= site_url('rksp/sbs/tambah/komoditas') ?>" class="btn btn-flat btn-default" data-toggle="tooltip" title="" data-original-title="Tambah RKSP Sayuran dan Buah-buahan Semusim berdasarkan Komoditas"><i class="fa fa-briefcase"></i> Berdasarkan Komoditas</a>
                    <a href="<?= site_url('rksp/sbs/tambah/kota') ?>" class="btn btn-flat btn-default" data-toggle="tooltip" title="" data-original-title="Tambah RKSP Sayuran dan Buah-buahan Semusim berdasarkan Kota"><i class="fa fa-location-arrow"></i> Berdasarkan Daerah</a>
                </div><!-- /. tools -->
            </div>            
        </div>        
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-info">
            <div class="box-header">
                <i class="fa fa-file-text"></i>
                <h3 class="box-title">Daftar RKSP SBS</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">  
                    
                </div><!-- /. tools -->
            </div>
            <div class="box-body">

            </div>            
        </div>
    </div>
</div>