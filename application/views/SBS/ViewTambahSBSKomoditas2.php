<div class="row">
    <div class="col-lg-12">
        <div class="box box-info">
            <div class="box-header">
                <i class="fa fa-file-text"></i>
                <h3 class="box-title">Mengisi Detail RKSP SBS</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
					<button class="form-control btn btn-flat btn-primary" data-toggle="tooltip" title="" data-original-title="Cek Dokumen RKSP"><i class="fa fa-search"></i> Cek Dokumen RKSP</a>
				</div><!-- /. tools -->
            </div>
            <div class="box-body">                
				<form>
					<div class="row">
						<div class="col-lg-4">						
							<div class="form-group">
								<label for="komoditas">Komoditas</label>
								<select class="form-control" id="komoditas" name="komoditas">
									<option>Pilih Salah Satu</option>
								</select>						
							</div>
						</div>						
						<div class="col-lg-2">
							<div class="form-group">
								<label for="komoditas">Tahun</label>
								<select class="form-control" id="tahun" name="tahun">
									<option>Pilih Salah Satu</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label for="bulan">Bulan</label>
								<input type="hidden" name="bulan" value=""/>
								<p class="form-control-static">Januari</p>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label for="komoditas">Provinsi</label>
								<input type="hidden" name="id_daerah" value=""/>
								<p class="form-control-static">Kalimantan Tengah</p>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-lg-12">							
							<b>Status Dokumen</b>
							<p class="help-block">Belum diketahui, silahkan isi form dan pilih tombol "Cek Dokumen RKSP" untuk mengecek keberadaan dokumen</p>
						</div>												
					</div>										
				</form>					
			</div> 
		</div>
	</div>
</div>            