<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>si.din.tan | <?=$page_title?></title>

        <!-- Google Fonts embed code -->
        <script type="text/javascript">
            (function() {
                var link_element = document.createElement("link"),
                    s = document.getElementsByTagName("script")[0];
                if (window.location.protocol !== "http:" && window.location.protocol !== "https:") {
                    link_element.href = "http:";
                }
                link_element.href += "//fonts.googleapis.com/css?family=Titillium+Web:200italic,200,300italic,300,400italic,400,600italic,600,700italic,700,900";
                link_element.rel = "stylesheet";
                link_element.type = "text/css";
                s.parentNode.insertBefore(link_element, s);
            })();
        </script>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?= base_url('css/bootstrap/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('css/font-awesome/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />        
        <!-- Ionicons -->
        <link href="<?= base_url('css/ionicons/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />        
        <!-- Morris chart -->
        <link href="<?= base_url('css/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?= base_url('css/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="<?= base_url('css/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?= base_url('css/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?= base_url('css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?= base_url('css/AdminLTE.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Custom style -->
        <link href="<?= base_url('css/main.css') ?>" rel="stylesheet" type="text/css" />        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?= base_url('js/plugins/jquery/jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('js/plugins/bootstrap/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('js/plugins/jquery-ui/jquery-ui.min.js') ?>" type="text/javascript"></script>    
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <?php $this->load->view('ViewHeader') ?>        

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php $this->load->view('Dashboard/ViewMenu') ?>   
                
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?=$title?>
                        <small><?=$subtitle?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <?php $i=0; foreach($breadcrumb as $link => $title): ?>
                            <?php if($link==base_url()): ?>
                                <li><a href="<?=$link?>"><i class="fa fa-dashboard"></i> <?=$title?></a></li>
                            <?php else: ?>
                                <?php if($link=='#'): ?>
                                    <li class="active"><?=$title?></li>
                                <?php else: ?>
                                    <li><a href="<?=$link?>"><?=$title?></a></li>
                                <?php endif; ?>
                            <?php endif; ?>                            
                        <?php endforeach; ?>                        
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php $this->load->view($page, $detail) ?>   
                </section><!-- /.content -->        
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->                

        <!-- iCheck -->
        <script src="<?= base_url('js/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="<?= base_url('js/AdminLTE/app.js') ?>" type="text/javascript"></script>        

    </body>
</html>