<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelOpsi extends CI_Model {

	private $minTahun, $maxTahun;
	public $bulan = array("Januari","Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	public function __construct()
	{
		parent::__construct();
		$this->minTahun = 2000;
		$this->maxTahun = date('Y');
		
	}

	public function opsiKomoditas($idKategori, $selectedKomoditas=NULL){
		$this->load->model('ModelKomoditas', 'komoditas');
		$komoditas = $this->komoditas->ambilKomoditas($idKategori);

		$o = $this->opsiPertama();
		foreach($komoditas as $i=>$k){
			$isSelected = FALSE;

			if($k['id_komoditas'] == $selectedKomoditas)
				$isSelected = TRUE;
			
			$o.= $this->opsi($k['id_komoditas'], $k['komoditas'], $isSelected);
		}
		return $o;
	}

	public function opsiBulan($selectedBulan=NULL){
		$o = $this->opsiPertama();
		foreach($this->bulan as $i=>$b){
			$val = $i+1;
			$isSelected = FALSE;

			if($val == $selectedBulan)
				$isSelected = TRUE;
			
			$o.= $this->opsi($val, $b, $isSelected);
		}
		return $o;
	}

	public function opsiTahun($selectedTahun=NULL){
		$o = $this->opsiPertama();
		for($i=$this->minTahun; $i<=$this->maxTahun; $i++){
			$isSelected = FALSE;

			if($i == $selectedTahun)
				$isSelected = TRUE;
			
			$o.= $this->opsi($i, $i, $isSelected);
		}
		return $o;
	}

	public function opsi($value, $option, $isSelected=FALSE){
     	$selected = "";

     	if($isSelected)
     		$selected = 'selected="selected"';

     	$o = "<option value=\"$value\" $selected>$option</option>";

     	return $o;
	}

	public function opsiPertama($option=NULL){  
		$o = "";

		if(empty($option))
     		$o = "<option value=\"\">Pilih Salah Satu</option>";
     	else
     		$o = "<option value=\"\">$option</option>";

     	return $o;
	}

}

/* End of file ModelOpsi.php */
/* Location: ./application/models/ModelOpsi.php */