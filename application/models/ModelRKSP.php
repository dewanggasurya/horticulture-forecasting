<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelRKSP extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}
									
	public function tambahRKSPDaerah($tahun, $periode, $komoditas, $daerah, $kolom, $kategori, $pengguna, $rksp)
	{	
		$this->db->trans_begin();
		$q1 = "INSERT INTO tb_rksp (tahun, periode, id_kategori, id_pengguna, tgl_catat) ";	
		$q1.= "VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP)";

		$r1 = $this->db->query($q1, array($tahun, $periode, $kategori, $pengguna));
		$id_rksp = $this->db->insert_id();

		if($this->db->affected_rows()>0){		

			foreach ($daerah as $i=>$d){

				if($this->db->affected_rows()>0){
					
					$x = $i+1;
					foreach ($kolom as $j=>$k) {						
						
						$y = $j+2;
						if(array_key_exists($x, $rksp)){
							if(array_key_exists($y, $rksp[$x])){
								$q2 = "INSERT INTO tb_detail_rksp (id_rksp, id_daerah, id_kolom_rksp, id_komoditas, nilai, tgl_catat)";
								$q2.= "VALUES(?,?,?,?,?, CURRENT_TIMESTAMP)";
								$r2 = $this->db->query($q2, array($id_rksp, $d['id_daerah'], $k['id_kolom_rksp'], $komoditas, $rksp[$x][$y]));
							}

						}

					}	


				}	

			}
		}else{
			$this->db->trans_rollback();
			return false;
		}
		
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		    return false;
		}else{
		    $this->db->trans_commit();
		    return true;
		}	
		
	}

}

/* End of file ModelRKSP.php */
/* Location: ./application/models/ModelRKSP.php */