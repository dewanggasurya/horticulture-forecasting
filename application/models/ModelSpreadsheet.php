<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelSpreadsheet extends CI_Model {
	const EXIST 		= 1;
	const LASTMONTH_EXIST 	= 2;
	const LASTMONTH_NOT_EXIST = 3;
	const NOT_EXIST 	= 4;
	

	public function __construct()
	{
		parent::__construct();		
	}

	public function ambilKolom($kategori){
		$q = "SELECT k.id_kolom_rksp, k.tipe_data, k.kode AS kode_kolom, k.kolom AS nama, s.id_satuan, s.kategori, s.kode AS kode_satuan, s.nama AS satuan, s.singkatan ";
		$q.= "FROM tb_kolom_rksp k ";
		$q.= "LEFT JOIN tb_satuan s USING(id_satuan) ";
		$q.= "WHERE k.id_kategori = ? ";
		$q.= "ORDER BY k.urutan";

		$r = $this->db->query($q, $kategori);

		return $r->result_array();
	}

	public function ambilKomoditas($kategori){
		$q = "SELECT k.id_komoditas, k.nama, sk.id_kolom_rksp ";
		$q.= "FROM tb_komoditas k ";
		$q.= "LEFT JOIN tb_satuan_komoditas sk USING(id_komoditas) ";
		$q.= "WHERE k.id_kategori = ? ";
		$q.= "GROUP BY k.id_komoditas ";
		$q.= "ORDER BY k.urutan";		

		$r = $this->db->query($q, $kategori);

		return $r->result_array();
	}

	public function ambilDaerah($tingkat){
		$q = "SELECT IF(jenis = '2',CONCAT('Kota ', nama),nama) AS nama, id_daerah  ";
		$q.= "FROM tb_daerah ";
		$q.= "WHERE tingkat = ? ";		
		$q.= "ORDER BY urutan";

		$r = $this->db->query($q, $tingkat);

		return $r->result_array();
	}

	public function ambilIsiTabelDaerah($tahun, $periode, $komoditas, $daerah, $kategori){		
		$q1 = "SELECT DISTINCT r.id_rksp, r.tgl_catat, r.tgl_ubah, p.id_pengguna, p.nama ";
		$q1.= "FROM tb_rksp r, tb_pengguna p, tb_detail_rksp d, tb_daerah da ";
		$q1.= "WHERE r.id_pengguna=p.id_pengguna ";
		$q1.= "AND r.id_rksp=d.id_rksp ";		
		$q1.= "AND d.id_daerah=da.id_daerah ";		
		$q1.= "AND r.tahun=? ";
		$q1.= "AND r.periode=? ";
		$q1.= "AND r.id_kategori=? ";		
		$q1.= "AND da.id_daerah_atas=? ";

		$rksp = array();

		//Mengecek apakah RKSP telah ada atau tidak
		$r1 = $this->db->query($q1, array($tahun, $periode, $kategori, $daerah));
		if($r1->num_rows() > 0){
			//Ketika RKSP ada, maka sistem akan diambil seluruh data dari RKSP
			$d = $r1->row_array();

			$rksp['status'] = self::EXIST;
			$rksp['id_rksp'] = $d['id_rksp'];
			$rksp['tgl_catat'] = $d['tgl_catat'];
			$rksp['tgl_ubah'] = $d['tgl_ubah'];
			$rksp['id_pengguna'] = $d['id_pengguna'];
			$rksp['nama'] = $d['nama'];

			$id_rksp = $d['id_rksp'];

			$q2 = "SELECT d.id_detail_rksp AS id, d.nilai, d.id_daerah, d.id_komoditas, d.id_kolom_rksp ";
			$q2.= "FROM tb_detail_rksp d ";
			$q2.= "LEFT JOIN tb_komoditas km USING(id_komoditas) ";
			$q2.= "LEFT JOIN tb_kolom_rksp kr USING(id_kolom_rksp) ";
			$q2.= "LEFT JOIN tb_daerah da USING(id_daerah) ";
			$q2.= "WHERE d.id_rksp=? ";
			$q2.= "ORDER BY da.urutan, kr.urutan ";

			$r2 = $this->db->query($q2, array($id_rksp));
			$rksp['tabel'] = $r2->result_array();			
		}else{
			//Ketika tidak ditemukan, maka sistem akan mengambil data lahan bulan lalu			
			$q3 = "SELECT d.nilai ";
			$q3.= "FROM tb_rksp r ";
			$q3.= "LEFT JOIN tb_detail_rksp d USING(id_rksp) ";
			$q3.= "LEFT JOIN tb_komoditas km USING(id_komoditas) ";
			$q3.= "LEFT JOIN tb_kolom_rksp kr USING(id_kolom_rksp) ";
			$q3.= "INNER JOIN tb_daerah da USING(id_daerah) ";

			$urutan  = NULL;
			$periodeSebelum = NULL;

			if($kategori == '1'){				
				$urutan  = 6;
			}else if($kategori == '2'){
				$urutan  = 8;
			}else if($kategori == '3'){
				$urutan  = 6;
			}else if($kategori == '4'){
				$urutan  = 6;
			}else{
				$rksp['status'] = self::NOT_EXIST;
			}

			$waktu = $this->periodeSebelum($periode, $tahun, $kategori);
			$q3.= "WHERE kr.urutan ='6' ";
			$q3.= "AND r.periode = ? ";	
			$q3.= "AND r.tahun = ? ";
			$q3.= "ORDER BY da.urutan, kr.urutan ";

			$r3 = $this->db->query($q3, array($waktu['periode'], $waktu['tahun']));
			if($r3->num_rows() > 0){	
				//Jika data bulan lalu ada, maka akan ditampilkan				
				$rksp['status'] = self::LASTMONTH_EXIST;
				$rksp['tabel'] = $r3->result_array();
			}else{
				//Jika data bulan lalu tidak ada, maka akan dikosongkan
				$rksp['status'] = self::LASTMONTH_NOT_EXIST;
				$rksp['tabel'] = NULL;
			}
			
		}

		return json_encode($rksp);
	}
	private function periodeSebelum($periode, $tahun, $kategori){
		$periode--;
		
		if($kategori == '1'){
			if($periode<0){
				$periode=12;
				$tahun--;
			}
		}

		if($kategori == '2' || $kategori == '3' || $kategori == '4'){
			if($periode<0){
				$periode=4;
				$tahun--;
			}
		}	

		return array('periode'=>$periode, 'tahun'=>$tahun);
	}	

	public function ambilDefaultTabelKomoditas($kategori){
		$json = array(
					"kolom" => $this->ambilKolom($kategori),
					"komoditas" => $this->ambilKomoditas($kategori)
				);

		return json_encode($json);
	}

	public function ambilDefaultTabelDaerah($kategori, $tingkat){
		$json = array(
					"kolom" => $this->ambilKolom($kategori),
					"daerah" => $this->ambilDaerah($tingkat)
				);

		return json_encode($json);
	}

}

/* End of file ModelSpreadsheet.php */
/* Location: ./application/models/ModelSpreadsheet.php */