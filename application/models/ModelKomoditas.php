<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelKomoditas extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function ambilKomoditas($idKomoditas=NULL){
		$q = "SELECT kom.id_komoditas, kom.nama AS komoditas, kat.id_kategori, kat.nama AS kategori ";
		$q.= "FROM tb_komoditas kom, tb_kategori kat ";
		$q.= "WHERE kom.id_kategori = kat.id_kategori ";
		if(!empty($idKomoditas)){			
			$q.= "AND kom.id_kategori = ? ";
		}
		$q.= "ORDER BY kom.urutan";

		$r = NULL;
		if(!empty($idKomoditas)){
			$r = $this->db->query($q, $idKomoditas);
		}else{
			$r = $this->db->query($q);
		}

		return $r->result_array();
	}

}

/* End of file ModelKomoditas.php */
/* Location: ./application/models/ModelKomoditas.php */