<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rksp extends CI_Controller {

    private $main_page = 'ViewMain';

    public function sbs($index = NULL, $method = NULL, $step = NULL) {
        $data = array();
        $idKategori = 1;
        switch ($index) {
            case 'tambah':
                switch ($method) {
                    //Menu untuk menambahkan RKSP SBS berdasarkan Komoditas
                    case 'komoditas':
                        $data['page_title'] = 'Menambah Rekapitulasi Kabupaten Statistik Pertanian Sayuran dan Buah-buahan Semusim Baru';
                        $data['title'] = 'Menambah RKSP Baru';
                        $data['breadcrumb'] = array(
                            base_url() => 'Dashboard',
                            site_url('rksp') => 'RKSP',
                            site_url('rksp/sbs') => 'SBS',
                            '#' => 'Tambah RKSP SBS',
                        );
                        if($step==1 || empty($step)){
                            $this->load->model('ModelOpsi', 'opsi');

                            $data['subtitle'] = 'Langkah 1';
                            $data['page'] = 'SBS/ViewTambahSBSKomoditas1';  
                            $data['tahun'] = $this->opsi->opsiTahun(2014);
                            $data['bulan'] = $this->opsi->opsiBulan();
                            $data['komoditas'] = $this->opsi->opsiKomoditas($idKategori);
                            $data['detail'] = NULL;
                        }elseif($step==2){
                            $data['subtitle'] = 'Langkah 2';
                            $data['page'] = 'SBS/ViewTambahSBSKomoditas2';
                            $data['detail'] = NULL;
                        }
                        break;
                }
                break;
            default:
                $data['page'] = 'SBS/ViewSBS';
                $data['title'] = 'Daftar RKSP';
                $data['subtitle'] = 'SBS';
                $data['page_title'] = 'Daftar Rekapitulasi Kabupaten Statistik Pertanian Sayuran dan Buah-buahan';                
                $data['breadcrumb'] = array(
                    base_url() => 'Dashboard',
                    site_url('rksp') => 'RKSP',
                    '#' => 'SBS'
                );
                $data['detail'] = NULL;

                break;
        }

        $this->load->view($this->main_page, $data);
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */