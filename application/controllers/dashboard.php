<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    private $main_page = 'ViewMain';

    public function index() {
        $data['page'] = 'Dashboard/ViewDashboard';
        $data['page_title'] = 'Sistem Informasi Pertanian (Tanaman Hortikultura)';
        $data['title'] = 'Rekapitulasi Data';
        $data['subtitle'] = 'Tanaman Hortikultura';
        $data['breadcrumb'] = array(
            '#' => 'Dashboard'
        );
        $data['detail'] = NULL;
        $this->load->view($this->main_page, $data);
    }

    public function halo(){
        $data['page'] = 'Dashboard/ViewLogin';
        $data['page_title'] = 'Sistem Informasi Pertanian (Tanaman Hortikultura)';
        $data['title'] = 'Selamat Datang';        
        $data['detail'] = NULL;
        $this->load->view($this->main_page, $data);
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */