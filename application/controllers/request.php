<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function isiDefaultTabelKomoditas($kategori)
	{
		$this->load->model('ModelSpreadsheet', 'sp');
		$response = $this->sp->ambilDefaultTabelKomodita($kategori);
		echo $response;
	}

	public function isiDefaultTabelDaerah()
	{
		$kategori = $this->input->post('kategori');
		$tingkat = $this->input->post('tingkat');

		$this->load->model('ModelSpreadsheet', 'sp');
		$response = $this->sp->ambilDefaultTabelDaerah($kategori, $tingkat);
		echo $response;
	}

	public function isiTabelDaerah()
	{
		$tahun = $this->input->post('tahun');
		$komoditas = $this->input->post('komoditas');
		$periode = $this->input->post('periode');
		$daerah = $this->input->post('daerah');
		$kategori = $this->input->post('kategori');

		$this->load->model('ModelSpreadsheet', 'sp');
		$response = $this->sp->ambilIsiTabelDaerah($tahun, $periode, $komoditas, $daerah, $kategori);

		echo $response;
	}

	public function simpanRKSPDaerah(){
		$tahun = $this->input->post('tahun');
		$komoditas = $this->input->post('komoditas');
		$periode = $this->input->post('bulan');
		$kategori = $this->input->post('kategori');
		$tingkat = $this->input->post('tigkat');
		$pengguna = $this->input->post('pengguna');
		$rksp = json_decode($this->input->post('rksp'));

		$this->load->model('ModelSpreadsheet', 'sp');
		$kolom = $this->sp->ambilKolom($kategori);		
		$daerah = $this->sp->ambilDaerah($tingkat);

		$this->load->model('ModelRKSP', 'rksp');
		$result = $this->rksp->tambahRKSPDaerah($tahun, $periode, $komoditas, $daerah, $kolom, $kategori, $pengguna, $rksp);
		
		$response['status'] = null;

		if($result){
		
			$response['status'] = 1;

		}else{

			$response['status'] = 0;

		}

		echo json_encode($response);

		//echo $response;
	}

}

/* End of file request.php */
/* Location: ./application/controllers/request.php */